==========
iMesh API
==========

.. ctype:: iRel_Object

   This subtype of :ctype:`iRel_Object` represents an iRel instance object.

.. ctype:: iRelPair_Object

   This subtype of :ctype:`PyObject` represents an iRel pair object.

.. ctype:: iRelPairSide_Object

   This subtype of :ctype:`PyObject` represents one side of an iRel pair object.

.. cvar:: PyTypeObject iRel_Type

   This instance of :ctype:`PyTypeObject` represents the iRel instance type.
   This is the same object as :class:`itaps.iRel.Rel`.

.. cvar:: PyTypeObject iRelPair_Type

   This instance of :ctype:`PyTypeObject` represents the iRel pair type.
   This is the same object as :class:`itaps.iRel.Pair`.

.. cvar:: PyTypeObject iRelPairSide_Type

   This instance of :ctype:`PyTypeObject` represents the iRel pair side type.
   This is the same object as :class:`itaps.iRel.PairSide`.

.. cfunction:: int iRel_Check(PyObject *p)

   Return true if its argument is a :class:`~itaps.iRel.Rel` or a subtype of
   :class:`~itaps.iRel.Rel`.

.. cfunction:: int iRelPair_Check(PyObject *p)

   Return true if its argument is a :class:`~itaps.iRel.Pair` or a subtype of
   :class:`~itaps.iRel.Pair`.

.. cfunction:: int iRelPairSide_Check(PyObject *p)

   Return true if its argument is a :class:`~itaps.iRel.PairSide` or a subtype
   of :class:`~itaps.iRel.PairSide`.

.. cfunction:: PyObject* iRelPair_New()

   Return a new uninitialized :class:`~itaps.iRel.Pair`, or *NULL* on
   failure.

.. cfunction:: PyObject* iRelPairSide_New()

   Return a new uninitialized :class:`~itaps.iRel.PairSide`, or *NULL* on
   failure.

.. cfunction:: PyObject* iRel_FromInstance(iMesh_Instance instance)

   Return a new (unowned) :class:`~itaps.iRel.Rel`, or *NULL* on failure.

.. cfunction:: PyObject* iMeshEntitySet_FromHandle(iMesh_Object *m, iBase_EntitySetHandle h)

   Return a new :class:`itaps.iMesh.EntitySet` from a
   :class:`~itaps.iMesh.Mesh` object and a C ``iBase_EntitySetHandle``, or
   *NULL* on failure.

.. cfunction:: iRel_Object* iRelPair_GET_INSTANCE(PyObject *p)

   Return the :class:`~itaps.iRel.Rel` object of the object *p*. No error
   checking is performed.

.. cfunction:: iRel_PairHandle iRelPair_GET_HANDLE(PyObject *p)

   Return the pair handle the object *p*. No error checking is performed.
