============
 An Example
============

Below is a short (but complete) example of how one might use PyTAPS. The example
accepts two mesh files (one pre- and one post-deformation) and calculates the
volumes of the regions in each mesh. From there, it then determines the ratios
of the volumes of each region and graphs them (or optionally prints the raw data
to the console).

.. literalinclude :: ../../examples/volume.py
