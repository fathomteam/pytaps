#include <Python.h>
#include <stdio.h>
#include <../../iMesh_Python.h>

static PyObject *
print_mesh(PyObject *self, PyObject *args, PyObject *kw)
{
    iMesh_Object *tmp;
    iMesh_Instance mesh;
    static char *kwlist[] = {"instance",0};
    if(!PyArg_ParseTupleAndKeywords(args,kw,"O!",kwlist,&iMesh_Type,&tmp))
        return NULL;

    mesh = tmp->handle;

    printf("mesh = %p\n", mesh);
    Py_RETURN_NONE;
}

static PyMethodDef methods[] = {
    {"print_mesh", print_mesh, METH_KEYWORDS, "Greet the world!"},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
inititaps_ext_example(void)
{
    PyObject *m;
    m = Py_InitModule("itaps_ext_example", methods);
    if (m == NULL)
        return;

    import_array();
    import_iBase();
    import_iMesh();
}
